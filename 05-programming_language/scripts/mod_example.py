import netaddr

# http://netaddr.readthedocs.io/en/latest/

ip = netaddr.IPAddress('192.0.2.1')
print '\n\n'
print ip.version
print ip.netmask_bits()
print ip.is_private()
print ip.is_multicast()

subnetip = netaddr.IPNetwork('10.10.10.0/30')
print subnetip.network
print subnetip.broadcast

print '\n\n'
mgmt_ip_list = []
ip_range = netaddr.IPNetwork('10.10.10.0/30')
for ip in ip_range.iter_hosts():
    mgmt_ip_list.append(ip)

print mgmt_ip_list

for ip in mgmt_ip_list:
    print ip