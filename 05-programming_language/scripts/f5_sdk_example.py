
try:
    from f5.bigip import ManagementRoot
except ImportError:
    print 'F5-sdk module not found'

import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Connect to the BigIP
mgmt = ManagementRoot("192.168.0.145", "admin", "password123")
nodes = mgmt.tm.ltm.nodes.get_collection()
pools = mgmt.tm.ltm.pools.get_collection()
vservers = mgmt.tm.ltm.virtuals.get_collection()
snatpool = mgmt.tm.ltm.snatpools.get_collection()
irules = mgmt.tm.ltm.rules.get_collection()
monitors = mgmt.tm.ltm.monitor.get_collection()
profiles = mgmt.tm.ltm.profile.get_collection()

# Display nodes
print '\n\n'
for node in nodes:
    print node.name
