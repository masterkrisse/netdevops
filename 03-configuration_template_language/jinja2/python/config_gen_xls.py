# import modules
import pandas as pd
from jinja2 import Template
import sys


def config_gen(xlfilename, sheetname, j2template, keyvar):
    xls_file = pd.ExcelFile(xlfilename)
    df = xls_file.parse(sheetname)
    col_names = list(df)
    for col_name in col_names:
        df[col_name] = pd.Series(df[col_name]).fillna(method='ffill')
    dev_indx = df[keyvar].tolist()
    data_into_dict = df.to_dict(orient='records')

    devname = ''
    for item in data_into_dict:
        with open(j2template) as conf_template:
            conf_data = conf_template.read()
        template = Template(conf_data)
        device_config = (template.render(item))
        # print item[keyvar]
        if item[keyvar] == devname:
            with open(('config_files/%s.conf' % item[keyvar]), 'a') as conf_gen:
                conf_gen.write(device_config)
            devname = item[keyvar]
        elif item[keyvar] != devname:
            with open(('config_files/%s.conf' % item[keyvar]), 'w') as conf_gen:
                conf_gen.write(device_config)
            devname = item[keyvar]
        print 'config generation is completed for device %s' % item[keyvar]


# config_gen('var file.xlsx', 'Sheet1', 'template.j2', 'hostname')
# config_gen('var file.xlsx', 'Sheet2', 'template.j2', 'site')
print str(sys.argv[0])
config_gen(str(sys.argv[1]), str(sys.argv[2]), str(sys.argv[3]), str(sys.argv[4]))
print str(sys.argv[1])

#usage guide
#python config_gen_xls.py Var_file.xlsx Sheet2 template.j2 site



