from jinja2 import Template

device_config_var = {'hostname': 'MAnc-sw-1a', 'intlist': ['0/0','0/1','0/2','0/3'] }

hostname = 'manc-sw-1a'

with open('py_example.j2') as conf_template:
    conf_data = conf_template.read()
template = Template(conf_data)
device_config = (template.render(device_config_var))
with open(('%s.conf' % hostname), 'w') as conf_gen:
    conf_gen.write(device_config)
print 'config generation is completed for device %s' % hostname
print device_config
