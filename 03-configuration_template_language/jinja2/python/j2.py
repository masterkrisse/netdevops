from jinja2 import Template

t1 = Template(
    "!---------------------------------\n"
    "hostname {{ var_hostname }}\n\n"
    "{% for ntnum in int_list1 %}\n"
    "interface FastEthernet{{ ntnum }}\n"
    "no ip address\n"
    "shutdown\n"
    "duplex auto\n"
    "speed auto\n"
    "{% endfor %}"
)

t2 = Template(
    "!---------------------------------\n"
    "hostname {{ var_hostname }}\n\n"
    "{% for intnum in int_list1 %}\n"
    "interface FastEthernet{{ intnum }}\n"
    "switchport vlan {{int_vlan1}}\n"
    "no shutdown\n"
    "duplex auto\n"
    "speed auto\n"
    "{% endfor %}"
    "{% for intnum in int_list2 %}\n"
    "interface FastEthernet{{ intnum }}\n"
    "switchport vlan {{int_vlan2}}\n"
    "no shutdown\n"
    "duplex auto\n"
    "speed auto\n"
    "{% endfor %}"
)

outp1 = t1.render(var_hostname="manc-rtr-1a", int_list1=[5, 6, 7, 8, 9], int_vlan1=100, int_list2=[11, 15, 16, 17],
                  int_vlan2=200)
outp2 = t2.render(var_hostname="manc-rtr-1a", int_list1=[5, 6, 7, 8, 9], int_vlan1=100, int_list2=[11, 15, 16, 17],
                  int_vlan2=200)
print outp2

from jinja2 import Template

template = Template(
    "{% for  dev in devicelist %}\n"
    "hostname {{dev}}\n\n"
    "{% for int in intlist %}"
    "interface {{int}}\n\n"
    "{% endfor %}"
    "##### Next Devcie  #########\n"
    "{% endfor %}")

print template.render(devicelist=['leed-fw-1a', 'manc-fw-2'], intlist=['mgmt0', 'gi0/1', 'gi0/2'])

from jinja2 import Template

device_config_var = {'hostname': 'MAnc-sw-1a', 'intlist': ['0/0','0/1','0/2','0/3'] }

hostname = 'manc-sw-1a'

with open('py_example.j2') as conf_template:
    conf_data = conf_template.read()
template = Template(conf_data)
device_config = (template.render(device_config_var))
with open(('%s.conf' % hostname), 'w') as conf_gen:
    conf_gen.write(device_config)
print 'config generation is completed for device %s' % hostname
print device_config
