
from jinja2 import Template

template = Template(
    "{% for  dev in devicelist %}\n"
    "hostname {{dev}}\n\n"
    "{% for int in intlist %}"
    "interface {{int}}\n\n"
    "{% endfor %}"
    "##### Next Devcie  #########\n"
    "{% endfor %}")

print(template.render(devicelist=['leed-fw-1a', 'manc-fw-2'], intlist=['mgmt0', 'gi0/1', 'gi0/2']))
