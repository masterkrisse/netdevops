#!/usr/bin/python
# shettypratap75@gmail.com

OCUMENTATION = '''
---

module: get_xls_vars
short_description: Load variables from xls file
description:
    - User can load playbook variables from a Excel file
author: Prathap Shetty
requirements:
    - Pandas 0.19.2
    - xlrd 1.0.0
options:
    xlfilepath:
        description:
            - Name of xl file
        required: true
        default: null
        choices: []
        aliases: []

    sheetname:
        description:
            - Specfic sheet name
        required: false
        default: present
        choices: []
        aliases: []
    taskid: (TBC - Not yet implemented )
        description:
            - Specify the task id to get variables for just this task ref ID
        required: false
        default: present
        choices: []
        aliases: []

'''

EXAMPLES = '''
# ensure xls file exist in ../files  folder

- xls2vars: xlfilepath=xls_vars.xls sheetname=F5_VIPs

Module will retun xls_vars['sheetname']  dict/json data with all the variables

'''

import os

from ansible.module_utils.basic import *

try:
    HAS_PANDAS_XLRD = True
    import pandas as pd
    import xlrd

except ImportError as ie:
    HAS_PANDAS_XLRD = False


def import_data(xlfilename, sheetname=None):
    try:
        xls_file = pd.ExcelFile(xlfilename)
        get_sheet_names = pd.ExcelFile(xlfilename).sheet_names
    except IOError as err:
        raise ValueError("error reading file, {0}: {1}".format(xlfilename, err))
    except ValueError as err:
        raise ValueError("error reading file, {0}: Bad file format".format(xlfilename))
    data_in_dict = {}
    if sheetname is None:
        for sheet in get_sheet_names:
            df = xls_file.parse(sheet, convert_float=True)
            col_names = list(df)
            for col_name in col_names:
                df[col_name] = pd.Series(df[col_name]).fillna(value='')
            data_in_dict[sheet] = df.to_dict(orient='records')
    elif sheetname is not None:
        df = xls_file.parse(sheetname, convert_float=True)
        col_names = list(df)
        for col_name in col_names:
            df[col_name] = pd.Series(df[col_name]).fillna(value='')
        data_in_dict[sheetname] = df.to_dict(orient='records')
    return data_in_dict



def main():

    module = AnsibleModule(
        argument_spec=dict(
            xlfilepath=dict(type='str', required=True),
            sheetname=dict(type='str', required=False, default=None),
            taskid=dict(type='str', required=False, default='None'),

        ),
        supports_check_mode=True
    )
    if not HAS_PANDAS_XLRD:
        module.fail_json(msg='Ensure you have the Pandas and Xlrd modules installed',
                         error=str(ie))

    xlfilepath = module.params['xlfilepath']
    sheetname = module.params['sheetname']
    #taskid = module.params['taskid']

    xls_vars = import_data(xlfilepath, sheetname)

    module.exit_json(ansible_facts=xls_vars)

if __name__ == '__main__':
    main()
