provider "aws" {
  region = "eu-west-2"
  access_key = "xxx"
  secret_key = "xxxx"
}

resource "aws_instance" "web" {
  instance_type = "t2.micro"
  ami           = "ami-0a13d44dccf1f5cf6"

  # This will create 4 instances
  count = 4
}