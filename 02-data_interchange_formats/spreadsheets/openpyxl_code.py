from openpyxl import Workbook
from openpyxl import load_workbook

wb = Workbook()
# grab the active worksheet
ws = wb.active
# Data can be assigned directly to cells
ws['A1'] = 42
# Rows can also be appended
ws.append([1, 2, 3])
# Python types will automatically be converted
import datetime
ws['A2'] = datetime.datetime.now()
# Save the file
wb.save("sample_new.xlsx")
wb2 = load_workbook(filename = 'sample.xlsx')
print wb2.get_sheet_names()
load_sheet = wb2['sheet1']
print(load_sheet['A2'].value)