import pandas as pd
import json


# Load variable data from xls file
def import_data(xlfilename, sheetname=None):
    xls_file = pd.ExcelFile(xlfilename)
    get_sheet_names = pd.ExcelFile(xlfilename).sheet_names
    data_in_dict = {}
    if sheetname is None:
        for sheet in get_sheet_names:
            df = xls_file.parse(sheet, convert_float=True)
            col_names = list(df)
            for col_name in col_names:
                df[col_name] = pd.Series(df[col_name]).fillna(value='None')
            data_in_dict[sheet] = df.to_dict(orient='records')
    elif sheetname is not None:
        df = xls_file.parse(sheetname, convert_float=True)
        col_names = list(df)
        for col_name in col_names:
            df[col_name] = pd.Series(df[col_name]).fillna(value='None')
        data_in_dict[sheetname] = df.to_dict(orient='records')
    return data_in_dict


# For fun testing ONLY
xlfilepath = 'my_vars.xlsx'

f5_vars = import_data(xlfilepath)
# print f5_vars['sheet1']
print "Importing data from xl file: ----> {}\n".format(xlfilepath)

print "Data from following xl Sheets will be imported-->\n"
for item in f5_vars['sheet1']:
    print item

json_vars = json.dumps(f5_vars)

print json_vars

with open('my_vars.json', 'wb') as jsonfile:
    jsonfile.write(json_vars)
