import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = ['https://spreadsheets.google.com/feeds']

credentials = ServiceAccountCredentials.from_json_keyfile_name('your_google_D_api.json', scope)

gc = gspread.authorize(credentials)

wks = gc.open("net_automation_demo").sheet1
# Fetch a cell range
cell_list = wks.range('A2:A10')
for i in cell_list:
    print i.value
list_of_lists = wks.get_all_values()
for itm in list_of_lists:
    print itm

# Get all values from the first row
values_list = wks.row_values(1)
print values_list

# Get all values from the first column
values_list2 = wks.col_values(1)
print values_list2
