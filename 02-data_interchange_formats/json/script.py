import json


# JSON Decoding

with open('example_1.json', 'r') as json_file:
    json_decode = json.load(json_file)

print('\n')
print(json_decode)
print('\n')
print(type(json_decode))
print('\n')
for v in json_decode[0]['vlans']:
    print(v)

# JSON Encoding

py_dict = {
        'interface': 'Ethernet2/1',
        'vlan': '--',
        'type': 'eth',
        'portmode': 'routed',
        'state': 'up',
        'state_rsn_desc': 'none',
        'speed': '1000',
        'ratemode': 'D'
      }

json_endcode = json.dumps(py_dict)

print('\n')
print(type(json_endcode))
print('\n')
print(json_endcode)