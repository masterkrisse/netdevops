### LAB Jump Server
ssh host:  jumpserver01.netdevopstraining.co.uk
SSH PORT: 22 
default login : ubuntu
defautl password:  Before the class we will email you the ssh key
```

### Jupyter Notebook (iPython)

```buildoutcfg
url: http://ipython.netdevopstraining.co.uk:8888/
password: password123!
```

### Jenkins
```buildoutcfg

url :  http://jenkins01.netdevopstraining.co.uk:8080/

default login : netdevops
defautl password: password123!
 
```

### Ansible AWX

```buildoutcfg

url :  http://awx.netdevopstraining.co.uk

default login : netdevops 
defautl password: password123!
system default :  admin/password123!
```


### User credentials for  CSR1000V01 

```buildoutcfg
HOST = 172.31.12.76  or csr1000v01.netdevopstraining.co.uk
SSH_PORT = 22
NETCONF_PORT = 830
USER = netdevops
PASS = password123!
```
### User credentials for  CSR1000V02

```buildoutcfg
HOST = 172.31.12.77  or csr1000v02.netdevopstraining.co.uk
SSH_PORT = 22
NETCONF_PORT = 830
USER = netdevops
PASS = password123!

```

### User credentials for Cisco ASA

```buildoutcfg
HOST = 172.31.12.78  or asav01.netdevopstraining.co.uk
SSH_PORT = 22
HTTPS: 443
USER = netdevops
PASS = password123!

```

### User credentials for Cisco XRv9000

```buildoutcfg
HOST = 172.31.12.79  or xrv9000.netdevopstraining.co.uk
SSH_PORT = 22
NETCONF_PORT = 830
USER = netdevops
PASS = password123!

```

### User credentials for  remote F5 device

```buildoutcfg

HOST = https://f5v01.netdevopstraining.co.uk:8443   or 172.31.12.75
USER = admin
PASS = Pass294d123!

```
