*** Settings ***
Suite Setup  Connect  alias=${ALIAS}  host=${HOST}  port=${PORT}  username=${USERNAME}  password=${PASSWORD}  hostkey_verify=False
Suite Teardown  Close Session  alias=${ALIAS}
Library  Collections
Library  NcclientLibrary
Library  String
Library  XML

*** Variables ***
${USERNAME}  root
${PASSWORD}  D_Vay!_10&      
${HOST}  ios-xe-mgmt.cisco.com
${PORT}  10000
${ALIAS}  ncclient
*** Test Cases ***
Test Login
    ${config} =    Create Dictionary    host=ios-xe-mgmt.cisco.com    port=10000    username=root    password=D_Vay!_10&  hostkey_verify=False
    Connect    &{config}
    get_server_capabilities
    get_yang_modules
Sample-Test-Pass
    [Tags]  Sample
    [Documentation]  *Retrieve all or part of a specified configuration datastore.*
    ...    https://tools.ietf.org/html/rfc6241#section-7.1
    ${xml}= <aaa xmlns="http://tail-f.com/ns/aaa/1.1"> <authentication><users><user><name/></user></users></authentication></aaa>
    ${res}=  Get  ${ALIAS}   ${xml}
