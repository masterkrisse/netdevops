*** Settings ***
Library           NcclientLibrary

*** Variables ***
${ALIAS}  ncclient

*** Test Cases ***
Test Login
    ${config} =    Create Dictionary  alias=ncclient  host=ios-xe-mgmt.cisco.com    port=10000    username=root    password=D_Vay!_10&  hostkey_verify=False
    Connect    &{config}
    get_server_capabilities
    get_yang_modules
    ${config_out} =  get_config    alias=ncclient  source=running   filter_type=subtree   filter_criteria=<interfaces xmlns="urn:ietf:params:xml:ns:yang:ietf-interfaces"><interface></interface></interfaces>
    Log    ${config_out}
