# Demo Script to verify sanity of network

*** Settings ***
# Importing test libraries, resource files and variable files.
Library        pyats.robot.pyATSRobot
Library        genie.libs.robot.GenieRobot
Library        unicon.robot.UniconRobot

*** Variables ***
# Defining variables that can be used elsewhere in the test data.
# Can also be driven as dash argument at runtime

# Define the pyATS testbed file to use for this run
${testbed}     testbed.yml

*** Test Cases ***
# Creating test cases from available keywords.


# ---------  Initilize -----------
Initialize
    # Initializes the pyATS/Genie Testbed
    # pyATS Testbed can be used within pyATS/Genie
    use genie testbed "${testbed}"

    # Connect to both device
    connect to device "ip-172-31-12-76"


# ---------  Verify counts of features -----------
# Verify Bgp Neighbors
Verify the counts of Bgp 'established' neighbors for ip-172-31-12-76
    verify count "1" "bgp neighbors" on device "ip-172-31-12-76"


# Verify Bgp Routes
# Tags can be used to control the behavior of the tests, noncritical tests which
# fail, will not cause the entire job to fail

Verify the counts of Bgp routes for ip-172-31-12-76
    [Tags]    noncritical
    verify count "1" "bgp routes" on device "ip-172-31-12-76"

# Verify OSPF neighbor counts
Verify the counts of Ospf 'full' neighbors for ip-172-31-12-76
    verify count "0" "ospf neighbors" on device "ip-172-31-12-76"

# Verify Interfaces
Verify the counts of 'up' Interace for ip-172-31-12-76
    verify count "4" "interface up" on device "ip-172-31-12-76"


Send show version
    ${output}=    execute "show version" on device "ip-172-31-12-76"
    Should Contain  ${output}    Version 17.2.1r

Verify bgp all
    ${output}=    execute "show bgp all" on device "ip-172-31-12-76"
    Should match regexp  ${output}  10.20.30.0/24

