## Download Github cheat-sheet
```buildoutcfg
https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf

```
## Git command line instructions to setup a new repo

### Git global setup
```buildoutcfg
git config --global user.name "Classroom Demo"
git config --global user.email "classroom.demo@xxxxxxx"
```
### Create a new repository
```buildoutcfg
git clone http://gitlab.com/xxxxxx.git
cd my-test-project
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
```

### Existing folder
```buildoutcfg
cd existing_folder
git init
git remote add origin http://gitlab.com/xxxxxx.git
git add .
git commit -m "Initial commit"
git push -u origin master

```
### Existing Git repository
```buildoutcfg
cd existing_repo
git remote add origin http://gitlab.com/xxxxxx.git
git push -u origin --all
git push -u origin --tags
```
