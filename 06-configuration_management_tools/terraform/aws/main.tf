# Create a new instance
provider "aws" {
  region = "eu-west-2"
}

variable "subnet_id" {
  description = "ava_zone subnet id"

}

variable "availability_zone" {
  description = "AWS availability_zone"

}

variable "key_name" {
  description = "AWS key pair name"

}

variable "eip_alloc" {
  description = "Easy IP allocation"

}


resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.xxxxxx.id
  allocation_id = var.eip_alloc
}
*/


# based on saved custom ASA image with base config.

resource "aws_instance" "netdevops_ASAv_05" {
  ami           = "ami-07275a1e2acccbe1f"
  instance_type = "m4.large"
  key_name      =  var.key_name
  availability_zone  = var.availability_zone
  subnet_id = var.subnet_id
  private_ip = "172.31.12.78"  

  tags = {
    Name = "Cisco_ASAv0xxxx"
  }
}



