# config cisco asa
provider "ciscoasa" {
  api_url       = "https://asav01.netdevopstraining.co.uk"
  username      = "netdevops"
  password      = "Pass294d123!"
  ssl_no_verify = true
}

resource "ciscoasa_network_object" "ipv4host2" {
  name  = "ipv4_host2"
  value = "192.168.10.5"
}
resource "ciscoasa_network_object" "ipv4range2" {
  name  = "ipv4_range2"
  value = "192.168.10.5-192.168.10.15"
}
resource "ciscoasa_network_object" "ipv4_subnet2" {
  name  = "ipv4_subnet2"
  value = "192.168.10.128/25"
}

resource "ciscoasa_acl" "foo2" {
  name = "aclname2"
  rule {
    source              = "192.168.10.5/32"
    destination         = "192.168.15.0/25"
    destination_service = "tcp/443"
  }
  rule {
    source              = "192.168.10.0/24"
    source_service      = "udp"
    destination         = "192.168.15.6/32"
    destination_service = "udp/53"
  }
  rule {
    source              = "192.168.10.0/23"
    destination         = "192.168.12.0/23"
    destination_service = "icmp/0"
  }

   rule {
    source              = "192.168.22.0/23"
    destination         = "192.168.44.0/23"
    destination_service = "icmp/0"
  }
}