from netmiko import ConnectHandler
import getpass
import re
import socket

#########
class Device(object):
    def __init__(self, devicename, devicetype, port, **kwargs):
        self.devicename = devicename
        self.devicetype = devicetype
        self.port = port
        self.username = kwargs['username']
        self.password = kwargs['password']

    def connect(self):
        session_data = {}
        session_data['device_type'] = self.devicetype
        session_data['port'] = self.port
        session_data['ip'] = socket.gethostbyname(self.devicename)
        if self.devicetype == 'cisco_xr' or self.devicetype == 'cisco_ios' or self.devicetype == 'cisco_xe':
            session_data['global_delay_factor'] = .5
            session_data.update({'username': self.username, 'password': self.password})
        ssh_conn = ConnectHandler(**session_data)
        if self.devicetype == 'cisco_xe' or self.devicetype == 'cisco_ios':
            ssh_conn.send_command('terminal length 0')
        elif self.devicetype == 'cisco_xr':
            ssh_conn.send_command('terminal length 0')
            ssh_conn.send_command('terminal exec prompt no-timestamp')
        return ssh_conn  # run 'show interface brief'


def show_int_brief_xr(device_conn):
    if device_conn.device_type != 'cisco_xr':
        return None
    command = 'show interfaces brief'
    show_output = device_conn.send_command(command)
    return show_output


def show_int_brief_xe(device_conn):
    if device_conn.device_type != 'cisco_xe':
        return None
    command = 'show ip interface brief'
    show_output = device_conn.send_command(command)
    return show_output

def show_ip_route_xe(device_conn):
    if device_conn.device_type != 'cisco_xe':
        return None
    command = 'show ip route'
    show_output = device_conn.send_command(command)
    return show_output



#######################

print '#######'
#username = raw_input("username :  ")
#password = getpass.getpass("password:  ")
#auth = {'username': username, 'password': password}

auth = {'username': 'root', 'password': 'D_Vay!_10&'}

dev1 = Device('ios-xe-mgmt.cisco.com', 'cisco_xe', port='8181', **auth)

print '\n'
#print dev1.username
dev1_ssh = dev1.connect()
print dev1_ssh.device_type
print '\n'
dev1_int_bri = show_int_brief_xe(dev1_ssh)
dev1_ip_route = show_ip_route_xe(dev1_ssh)
print dev1_int_bri
print "\n\n"
print dev1_ip_route

