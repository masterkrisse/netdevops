
from netmiko import ConnectHandler
import re
cisco_csr1000v = {
    'device_type': 'cisco_xe',
    'ip':   'Ios-xe-mgmt.cisco.com',
    'username': 'root',
    'password': 'D_Vay!_10&',
    'port': 8181,          # optional, defaults to 22
    'secret': '',           # optional, defaults to ''
    'verbose': False,       # optional, defaults to False
}

net_connect = ConnectHandler(**cisco_csr1000v)
output1 = net_connect.send_command('show ip ospf')
print '\n\n'
print(output1)

config_commands = [ 'logging buffered 20040',
                    'no logging console' ]
output2 = net_connect.send_config_set(config_commands)
print(output2)
print '\n\n'
# You can regex to find str in show output and print
match = re.search(r'ospf [0-9]+', output1)
if match:
    print match.group()