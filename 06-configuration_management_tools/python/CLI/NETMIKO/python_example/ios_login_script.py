from netmiko import ConnectHandler
import getpass
import socket
import re


def logon(dname, dtype, auth):
    session_data = {}
    session_data['ip'] = socket.gethostbyname(dname)
    session_data['device_type'] = dtype
    if dtype == 'cisco_xr' or dtype == 'cisco_ios':
        session_data['global_delay_factor'] = .5
    session_data.update(auth)
    session = ConnectHandler(**session_data)
    session.send_command('terminal length 0')
    return session


def get_ipint_brif(session):
    if session.device_type != 'cisco_ios':
        return None
    command = 'show ip interface brif'
    output = session.send_command(command)
    return output


dname = 'router_name'
dtype = 'cisco_ios'

'''
Checkout the supported vendor here --- 

https://github.com/ktbyers/netmiko

'''

output_data = {dname: {'interface_list': ''}}
print '#######'
print 'Device Name: ', "\033[1;32;40m" + dname + "\033[0m"
print '#######'
username = raw_input("username :  ")
password = getpass.getpass("password:  ")
auth = {'username': username, 'password': password}

session = logon(dname, dtype, auth)
output_data[dname]['interface_list'] = get_ipint_brif(session)

print output_data[dname]['interface_list']

cleaned_output = re.sub(r'some-filter-text-CHANGEME', '', output_data[dname]['interface_list'])
with open(('%s_interface_list.txt' % dname),
          'w') as conf_gen:
    conf_gen.write(cleaned_output)
