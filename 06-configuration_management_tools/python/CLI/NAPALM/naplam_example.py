from napalm import get_network_driver
# driver = get_network_driver('junos')
# device = driver('192.168.0.88', 'root', 'juniper123')

driver = get_network_driver('nxos_ssh')
device = driver('192.168.0.76', 'admin', 'admin')

# print help(driver)
device.open()
# print help(device)

print device.get_facts()
print '\n\n'
print device.get_config()
#print device.get_bgp_neighbors()

# device.load_merge_candidate(filename='junos_vsrx_test.conf')
# device.load_merge_candidate(filename='nxos_test.conf')
# device.commit_config()