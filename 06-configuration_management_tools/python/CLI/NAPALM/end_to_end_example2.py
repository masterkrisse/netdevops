from napalm import get_network_driver
import  json
from jinja2 import Template

with open('variables2.json', 'r') as json_file:
    config_variables = json.load(json_file)

print config_variables

for device_conf in config_variables:

    with open('nxos_conf.j2') as conf_template:
        conf_data = conf_template.read()

    template = Template(conf_data)
    device_config = (template.render(device_conf))
    with open(('%s.conf' % device_conf['hostname']), 'w') as conf_gen:
        conf_gen.write(device_config)
    print 'config generation is completed for device %s' % device_conf['hostname']
    print device_config


    # driver = get_network_driver('junos')
    # device = driver('192.168.0.88', 'root', 'juniper123')

    driver = get_network_driver('nxos_ssh')
    device = driver(device_conf['mgmt_ip'], 'admin', 'admin')

    # print help(driver)
    device.open()
    # print help(device)

    # print device.get_facts()
    print '\n\n'
    # print device.get_config()
    #print device.get_bgp_neighbors()

    #device.load_merge_candidate(filename='junos_vsrx_test.conf')
    device.load_merge_candidate(filename='%s.conf' % device_conf['hostname'])
    device.commit_config()