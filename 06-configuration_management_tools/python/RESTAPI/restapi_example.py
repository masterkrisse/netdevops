import requests
import json

#python GET example
b = requests.session()
b.auth = ('admin', 'password123')
b.verify = False
b.headers.update({'Content-Type':'application/json'})
output = b.get('https://192.168.0.145/mgmt/tm/ltm/node')
print output.json()

#python POST example

pm = ['192.168.25.32:80', '192.168.25.33:80']
payload = { }
payload['kind'] = 'tm:ltm:pool:poolstate'
payload['name'] = 'rt-pool'
payload['members'] = [ {'kind': 'ltm:pool:members', 'name': member } for member in pm]
b.post('https://192.168.0.145/mgmt/tm/ltm/pool', data=json.dumps(payload))


