# Curl

```buildoutcfg
curl -k -u admin:Pass294d123 -X GET https://192.168.0.145/mgmt/tm/ltm/node  >node.json
curl -k -u admin:Pass294d123 -H “Content-Type: application/json” -X GET https://192.168.0.145/mgmt/tm/ltm/rule
```
#Postman
```buildoutcfg

GET https://192.168.0.145/mgmt/tm/ltm/node
GET https://192.168.0.145/mgmt/tm/ltm/pool
GET https://192.168.0.145/mgmt/tm/sys/file/ssl-cert

F5 API Ref- 
https://devcentral.f5.com/Wiki/iControlREST.APIRef_tm_ltm.ashx


ACI Calls
POST https://sandboxapicdc.cisco.com/api/aaaLogin.json
PAYLOAD --

{
"aaaUser" : {
"attributes" : {
"name" : "admin",
"pwd" : "ciscopsdt"
}
}
}

GET https://sandboxapicdc.cisco.com/api/class/fvTenant.json

```
# Python
```buildoutcfg

python restapi_example.py

```