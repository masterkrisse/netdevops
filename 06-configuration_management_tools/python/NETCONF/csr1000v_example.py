
from ncclient import manager

EDIT_INT_CONFIG_1 = """
<config>
   <interfaces xmlns = "urn:ietf:params:xml:ns:yang:ietf-interfaces">
     <interface>
      <name>GigabitEthernet3</name>
      <description>Routertricks_testing5</description>
      <enabled>true</enabled>
      </interface>
     </interfaces>
</config>
"""


EDIT_INT_CONFIG_2 = """
<config>
   <interfaces xmlns = "urn:ietf:params:xml:ns:yang:ietf-interfaces">
     <interface>
      <name>GigabitEthernet3</name>
      <description>Routertricks_testing5</description>
      <enabled>false</enabled>
      </interface>
     </interfaces>
</config>
"""


def csr_connect(host, port, user, password):
    return manager.connect(host=host,
                           port=port,
                           username=user,
                           password=password,
                           device_params={'name': "csr"},
                           timeout=30
            )

def edit_int_conf(conn):
        confstr = EDIT_INT_CONFIG_1
        rpc_obj = conn.edit_config(target='running', config=confstr)


def sendconf_csr(host, user, password):
    with csr_connect(host, port=10000, user=user, password=password) as m:
        edit_int_conf(m)

if __name__ == '__main__':
    #logging.basicConfig(level=logging.DEBUG)
    conft = sendconf_csr('ios-xe-mgmt.cisco.com', 'root', 'D_Vay!_10&')