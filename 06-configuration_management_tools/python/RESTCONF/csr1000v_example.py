


import requests

restconf_get_url = 'https://ios-xe-mgmt.cisco.com:9443/restconf/data/ietf-interfaces:interfaces'
restconf_get_url2 = 'https://ios-xe-mgmt.cisco.com:9443/restconf/data/openconfig-interfaces:interfaces'
username = 'root'
password = 'D_Vay!_10&'


def restconf_get(username, password):
    url = restconf_get_url2
    myheaders = {'content-type': 'application/json', 'Accept': 'application/yang-data+json'}
    response = requests.get(url, headers=myheaders, auth=(username, password), verify=False)
    return response


restconf_get_output = restconf_get(username=username, password=password)

print restconf_get_output.json()


import requests
import json

restconf_edit_url = 'https://ios-xe-mgmt.cisco.com:9443/restconf/data/ietf-interfaces:interfaces/interface=GigabitEthernet3'
username = 'root'
password = 'D_Vay!_10&'

payload = {"ietf-interfaces:interface": {"name": "GigabitEthernet3",
                                         "description": "routertricks.com_RESTCONF33455",
                                         "type": "iana-if-type:ethernetCsmacd",
                                         "enabled": True,
                                         "ietf-ip:ipv4": {"address": [
                                             {
                                                 "ip": "20.102.10.48",
                                                 "netmask": "255.255.255.0"
                                             }
                                         ]},
                                         "ietf-ip:ipv6": {}
                                         }
           }


def restconf_edit(username, password):
    url = restconf_edit_url
    myheaders = {'content-type': 'application/yang-data+json', 'Accept': 'application/yang-data+json'}
    response = requests.patch(url, data=json.dumps(payload), headers=myheaders, auth=(username, password),
                             verify=False)
    return response


restconf_put_output = restconf_edit(username=username, password=password)

print restconf_put_output
