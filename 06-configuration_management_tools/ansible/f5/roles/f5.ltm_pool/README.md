F5 LTM - Create/Update/Delete Pool
=========

Requirements
------------

None.

Role Variables
--------------

```yaml

---
# Variables guidelines -
# ltm_node_state:  "created" | "modified" | "deleted"
---
f5_ltm_pool:
  - {
      ltm_pool_name: "",
      ltm_pool_description: "",
      ltm_pool_lbmethod: "",
      ltm_pool_state: "",
      ltm_pool_monitor: ""
    }


```
Dependencies
------------
```
Ansible URI module

```
```yaml

Example Playbook
----------------
---
- name: Pool configuration
  hosts: f5lab
  gather_facts: no
  connection: local
  vars_prompt:
    - name: ltm_username
      prompt: "Enter username "
      private: no
    - name: ltm_password
      prompt: "Enter password "
      private: yes
    - name: ltm_provider
      prompt: "Enter provider "
      private: no

  roles:
    - ../f5.ltm_pool
```

License
-------

BSD

Author Information
------------------

* **Prathap Shetty** - *Network calls dev for ACI Fabric and F5 LTM* - [prathapshetty](https://github.com/prathapshetty)
