
### Gitlab - Generate a new SSH key

```buildoutcfg
ssh-keygen -t rsa -C "your_email@example.com" 

```
Add an SSH key by pasting the public key. Which is usually contained in the 
file '~/.ssh/id_rsa.pub' and begins with 'ssh-rsa'

### Clone netdevops-for-beginners git repo
```buildoutcfg
git clone git@gitlab.com:netdevopstraining/netdevops-for-beginners.git
```
### Run iPython notebook

```buildoutcfg
pip3 install jupyter 
jupyter notebook --generate-config
jupyter notebook password
jupyter notebook --ip=172.31.12.110 & 

jupyter notebook list 

(default it runs on port 8888)
you can change ipython password with this command: jupyter notebook password
```
### Code analysis for Python

```buildoutcfg
pylint your_script.py 

pylint --max-line-length=140 your_script.py 

pylint --const-rgx=[a-z]+ --max-line-length=140 your_script.py 

flake8 your_script.py  --config=setup.cfg
```
### Validates the YANG module 
```buildoutcfg
pyang -f tree ietf-ip.yang | head -

pyang -f tree ietf-ip.yang