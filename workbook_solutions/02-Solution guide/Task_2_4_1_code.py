import csv

# Read  CSV file
with open('example.csv', 'rb') as csvfile:
    csv_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in csv_reader:
        print row

with open('example.csv') as csvfile:
   reader = csv.DictReader(csvfile)
   for row in reader:
        print[row['hostname'], row['vlan']]


# create CSV file

with open('example2.csv', 'wb') as csvfile:
    csv_writer = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    csv_writer.writerow(['vrf_name'] + ['cust_name'] + ['bw'])
    csv_writer.writerow(['mgmt_vrf', 'RTDEMO', '10Mps'])
    csv_writer.writerow(['prod_vrf', 'RTDEMO', '1000Mps'])
    csv_writer.writerow(['test_vrf', 'RTDEMO', '100Mps'])