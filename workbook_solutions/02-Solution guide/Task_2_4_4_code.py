from openpyxl import Workbook
from openpyxl import load_workbook
import datetime
import yaml

wb = Workbook()
# grab the active worksheet
ws = wb.active
# Data can be assigned directly to cells
ws['A1'] = 42
# Rows can also be appended
ws.append([1, 2, 3])
# Python types will automatically be converted

ws['A2'] = datetime.datetime.now()
# Save the file
wb.save("sample.xlsx")
# Load workbook
book = load_workbook('Task_1_4_2_xl_file.xlsx')
print book.get_sheet_names()
sheet = book['sheet1']
key1 = sheet['A1'].value
print key1

xls_data = []
keys = [key1, sheet['B1'].value, sheet['C1'].value, sheet['D1'].value, sheet['E1'].value]
values = [sheet['A2'].value, sheet['B2'].value, sheet['C2'].value, sheet['D2'].value, sheet['E2'].value]

print keys
print values

create_dictionary = dict(zip(keys, values))
print create_dictionary
xls_data.append(create_dictionary)
print xls_data

yaml_endcode = yaml.safe_dump(xls_data)

with open('Task_1_4_4_xl_file.yml', 'wb') as ymlfile:
    ymlfile.write(yaml_endcode)
