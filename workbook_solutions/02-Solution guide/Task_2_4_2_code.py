from openpyxl import Workbook
from openpyxl import load_workbook

wb = Workbook()
# grab the active worksheet
ws = wb.active
# Data can be assigned directly to cells
ws['A1'] = 42
# Rows can also be appended
ws.append([1, 2, 3])
# Python types will automatically be converted
import datetime
ws['A2'] = datetime.datetime.now()
# Save the file
wb.save("sample.xlsx")

wb2 = load_workbook(filename = 'Task_1_4_2_xl_file.xlsx')
print wb2.get_sheet_names()
sheet = wb2['Sheet']

cells = sheet['A1': 'B5']

for c1, c2 in cells:
    print("{0:8} {1:8}".format(c1.value, c2.value))