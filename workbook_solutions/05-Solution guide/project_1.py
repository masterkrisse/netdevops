dataset1 = [
    {"hostname": "eulab-sw-01", "chassis_sn": "ANC34312122", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-25"},
    {"hostname": "eulab-sw-02", "chassis_sn": "ANC34312123", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-25"},
    {"hostname": "eulab-sw-03", "chassis_sn": "ANC34312125", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-25"},
    {"hostname": "eulab-sw-04", "chassis_sn": "ANC34312126", "slot_1": "N7K-F132XP-33", "slot_2": "N7K-F248XP-25"},
    {"hostname": "eulab-sw-05", "chassis_sn": "ANC34312127", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-25"},
    {"hostname": "eulab-sw-06", "chassis_sn": "ANC34312128", "slot_1": "N7K-F132XP-55", "slot_2": "N7K-F248XP-25"},
    {"hostname": "eulab-sw-07", "chassis_sn": "ANC34312129", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-25"},
    {"hostname": "eulab-sw-08", "chassis_sn": "ANC34312199", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-25"},
    {"hostname": "eulab-sw-09", "chassis_sn": "ANC34312124", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-25"},
    {"hostname": "uklab-sw-01", "chassis_sn": "ANC34312176", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-25"},
    {"hostname": "uklab-sw-02", "chassis_sn": "ANC34312453", "slot_1": "N7K-F132XP-34", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uklab-sw-03", "chassis_sn": "ANC34313443", "slot_1": "N7K-F132XP-43", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uklab-sw-04", "chassis_sn": "ANC34317667", "slot_1": "N7K-F132XP-12", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uklab-sw-05", "chassis_sn": "ANC34312345", "slot_1": "N7K-F132XP-44", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uklab-sw-06", "chassis_sn": "ANC34314352", "slot_1": "N7K-F132XP-34", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uklab-sw-07", "chassis_sn": "ANC34312876", "slot_1": "N7K-F132XP-33", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uklab-sw-08", "chassis_sn": "ANC34312334", "slot_1": "N7K-F132XP-30", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uklab-sw-09", "chassis_sn": "ANC34318787", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uklab-sw-10", "chassis_sn": "ANC34317767", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uklab-sw-11", "chassis_sn": "ANC34312879", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uklab-sw-12", "chassis_sn": "ANC34312347", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uklab-sw-13", "chassis_sn": "ANC34313457", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uklab-sw-14", "chassis_sn": "ANC34334543", "slot_1": "N7K-F132XP-56", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uslab-sw-01", "chassis_sn": "ANC34314562", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-26"},
    {"hostname": "uslab-sw-02", "chassis_sn": "ANC34345453", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-25"},
    {"hostname": "uslab-sw-03", "chassis_sn": "ANC34372356", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-25"},
    {"hostname": "uslab-sw-04", "chassis_sn": "ANC34318566", "slot_1": "N7K-F132XP-56", "slot_2": "N7K-F248XP-25"},
    {"hostname": "uslab-sw-05", "chassis_sn": "ANC34312742", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-25"},
    {"hostname": "uslab-sw-06", "chassis_sn": "ANC34315783", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-23"},
    {"hostname": "uslab-sw-07", "chassis_sn": "ANC34378345", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-23"},
    {"hostname": "uslab-sw-08", "chassis_sn": "ANC34312875", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-23"},
    {"hostname": "uslab-sw-09", "chassis_sn": "ANC34312674", "slot_1": "N7K-F132XP-56", "slot_2": "N7K-F248XP-23"},
    {"hostname": "uslab-sw-10", "chassis_sn": "ANC34312673", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-23"},
    {"hostname": "uslab-sw-11", "chassis_sn": "ANC34319687", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-23"},
    {"hostname": "uslab-sw-12", "chassis_sn": "ANC34387446", "slot_1": "N7K-F132XP-43", "slot_2": "N7K-F248XP-23"},
    {"hostname": "uslab-sw-13", "chassis_sn": "ANC34389506", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-23"},
    {"hostname": "uslab-sw-14", "chassis_sn": "ANC34324356", "slot_1": "N7K-F132XP-87", "slot_2": "N7K-F248XP-23"},
    {"hostname": "inlab-sw-01", "chassis_sn": "ANC34356684", "slot_1": "N7K-F132XP-76", "slot_2": "N7K-F248XP-23"},
    {"hostname": "inlab-sw-02", "chassis_sn": "ANC34367566", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F132XP-44"},
    {"hostname": "inlab-sw-03", "chassis_sn": "ANC34367456", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-23"},
    {"hostname": "inlab-sw-04", "chassis_sn": "ANC34318677", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-23"},
    {"hostname": "inlab-sw-05", "chassis_sn": "ANC34317845", "slot_1": "N7K-F132XP-56", "slot_2": "N7K-F248XP-23"},
    {"hostname": "inlab-sw-06", "chassis_sn": "ANC34318734", "slot_1": "N7K-F132XP-15", "slot_2": "N7K-F248XP-23"},
    {"hostname": "inlab-sw-07", "chassis_sn": "ANC34312675", "slot_1": "N7K-F132XP-66", "slot_2": "N7K-F248XP-23"},
    {"hostname": "inlab-sw-08", "chassis_sn": "ANC34314535", "slot_1": "N7K-F132XP-44", "slot_2": "N7K-F248XP-25"},
    {"hostname": "inlab-sw-09", "chassis_sn": "ANC34367456", "slot_1": "N7K-F132XP-44", "slot_2": "N7K-F248XP-25"},
    {"hostname": "inlab-sw-10", "chassis_sn": "ANC34334577", "slot_1": "N7K-F132XP-44", "slot_2": "N7K-F132XP-44"},
    {"hostname": "inlab-sw-11", "chassis_sn": "ANC34567743", "slot_1": "N7K-F132XP-44", "slot_2": "N7K-F132XP-44"}

]

search_for_mod = 'N7K-F132XP-15'
slot_1_mod_count = 0
slot_2_mod_count = 0
hostname_list = []

for i in dataset1:
    if i['slot_1'] == search_for_mod:
        slot_1_mod_count += 1
        hostname_list.append(i['hostname'])
    if i['slot_2'] == search_for_mod:
        slot_2_mod_count += 1
        hostname_list.append(i['hostname'])

print '\n'
print 'total ---> {}  {} module(s) found in slot-1 and {} in slot-2'.format(slot_1_mod_count, search_for_mod, slot_2_mod_count)
print '\n'
print list(set(hostname_list))


import csv
with open('Book1.csv', 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',')
    for row in spamreader:
        print row